package com.htc.ws.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.calculator.ErrorMessage;
import com.htc.calculator.GetDivisionRequest;
import com.htc.calculator.GetDivisionResponse;
import com.htc.calculator.GetMultiplicacionRequest;
import com.htc.calculator.GetMultiplicacionResponse;
import com.htc.calculator.GetRestaRequest;
import com.htc.calculator.GetRestaResponse;
import com.htc.calculator.GetSumaRequest;
import com.htc.calculator.GetSumaResponse;
import com.htc.exceptions.DivisionException;
import com.htc.service.CalculatorService;



@Endpoint
public class CalculatorEndpoint {

private static final String NAMESPACE_URI = "http://calculator.htc.com";
	
	private CalculatorService calculatorService;

	@Autowired
	public CalculatorEndpoint(CalculatorService calculatorService) {
		super();
		this.calculatorService = calculatorService;
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSumaRequest")
    @ResponsePayload
    public GetSumaResponse getState(@RequestPayload GetSumaRequest request) {
        GetSumaResponse response = new GetSumaResponse();
        response.setResultado( calculatorService.sumar(request.getNumber1(), request.getNumber2()) );
        return response;
    }
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getRestaRequest")
    @ResponsePayload
    public GetRestaResponse getState(@RequestPayload GetRestaRequest request) {
        GetRestaResponse response = new GetRestaResponse();
        response.setResultado( calculatorService.resta(request.getNumber1(), request.getNumber2()) );
        return response;
    }
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMultiplicacionRequest")
    @ResponsePayload
    public GetMultiplicacionResponse getState(@RequestPayload GetMultiplicacionRequest request) {
        GetMultiplicacionResponse response = new GetMultiplicacionResponse();
        response.setResultado( calculatorService.multiplicacion(request.getNumber1(), request.getNumber2()) );
        return response;
    }
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDivisionRequest")
    @ResponsePayload
    public GetDivisionResponse getState(@RequestPayload GetDivisionRequest request) throws DivisionException {
        GetDivisionResponse response = new GetDivisionResponse();
        try {
        response.setResultado( calculatorService.division(request.getNumber1(), request.getNumber2()) );
        
        }catch(DivisionException e) {
        	ErrorMessage error = new ErrorMessage();
        	error.setCode((int) e.getCode());
        	error.setMessage(e.getDescription());
        	response.setErrorMessage(error);        
        	return response;
        }
        return response;
    }
}
