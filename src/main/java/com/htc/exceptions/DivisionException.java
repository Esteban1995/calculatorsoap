package com.htc.exceptions;

public class DivisionException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private static long code;
	private static String description;

	public DivisionException(Exception ex) {
		super(ex);
		this.code= Constantes.ERROR_CODE_DIVISION;
		this.description = ex.getMessage();
	} 

	public DivisionException(String description, Exception ex) {
		super(ex);
		this.code=Constantes.ERROR_CODE_DIVISION;
		this.description=description;
	}
	
	public DivisionException(long code, String description, Exception ex) {
		super(ex);
		this.code=code;
		this.description=description;
	}
	
	public DivisionException(String description) {
		super();
		this.code=Constantes.ERROR_CODE_DIVISION;
		this.description=description;
	}
	
	@Override
	public String getMessage() {
		return toString();
	}
	
	public static long getCode() {
		return code;
	}

	public static String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Error=").append(code).append(", description=").append(description);
		return builder.toString();
	}	
}
