package com.htc.service.impl;

import org.springframework.stereotype.Component;

import com.htc.exceptions.DivisionException;
import com.htc.service.CalculatorService;

@Component
public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public double sumar(double n1, double n2) {
		return n1 + n2;
	}

	@Override
	public double resta(double n1, double n2) {
		return n1 - n2;
	}

	@Override
	public double multiplicacion(double n1, double n2) {
		return n1 * n2;
	}

	@Override
	public double division(double n1, double n2) throws DivisionException{
		if(n2 == 0) throw new DivisionException("El numero no puede ser dividido entre 0");
		
		return n1 / n2;
	}

}
