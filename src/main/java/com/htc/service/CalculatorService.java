package com.htc.service;

import com.htc.exceptions.DivisionException;

public interface CalculatorService {

	public double sumar(double n1, double n2); 
	
	public double resta(double n1, double n2); 
	
	public double multiplicacion(double n1, double n2); 
	
	public double division(double n1, double n2) throws DivisionException; 
}
